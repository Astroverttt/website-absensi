<?php
session_start();

// Ambil ID presensi dari parameter URL
$id_presensi = $_GET['id'];

// Ambil data presensi dari sesi berdasarkan ID
$presensi = null;
foreach ($_SESSION['presensi_mahasiswa'] as $p) {
    if ($p['id'] == $id_presensi) {
        $presensi = $p;
        break;
    }
}

// Proses pembaruan data presensi jika formulir disubmit
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Ambil data dari formulir (misalnya, $_POST['nama'], $_POST['tanggal'], dll.)
    $nama = $_POST['nama'];
    $tanggal = $_POST['tanggal'];
    $matkul = $_POST['matkul'];

    // Update data presensi di sesi
    foreach ($_SESSION['presensi_mahasiswa'] as &$p) {
        if ($p['id'] == $id_presensi) {
            $p['nama'] = $nama;
            $p['tanggal'] = $tanggal;
            $p['matkul'] = $matkul;
            break;
        }
    }

    // Redirect kembali ke halaman home_dosen.php atau halaman lain
    header("Location: home_dosen.php");
    exit();
}

// Proses pembaruan data presensi jika formulir delete disubmit
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['delete_id'])) {
    $delete_id = $_POST['delete_id'];

    // Hapus data presensi dari sesi
    foreach ($_SESSION['presensi_mahasiswa'] as $key => $presensi) {
        if ($presensi['id'] == $delete_id) {
            unset($_SESSION['presensi_mahasiswa'][$key]);
            // Redirect kembali ke halaman home_dosen.php atau halaman lain
            header("Location: home_dosen.php");
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Data Mahasiswa</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }

        header {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 1em;
        }

        section {
            margin: 1em;
        }

        form {
            max-width: 400px;
            margin: auto;
        }

        label {
            display: block;
            margin-bottom: 8px;
        }

        input {
            width: 100%;
            padding: 8px;
            margin-bottom: 16px;
        }

        button {
            background-color: #333;
            color: #fff;
            padding: 10px 15px;
            border: none;
            cursor: pointer;
        }

        .delete-form {
            display: inline-block;
        }

        .delete-button {
            background-color: #ff0000;
        }
    </style>
</head>
<body>
    <header>
        <h1>Edit Data Mahasiswa</h1>
    </header>
    <section>
        <form action="" method="post">
            <input type="hidden" name="edit_id" value="<?php echo $presensi['id']; ?>">
            <label>Nama Mahasiswa:</label>
            <input type="text" name="nama" value="<?php echo $presensi['nama']; ?>" required>

            <label>Tanggal Presensi:</label>
            <input type="date" name="tanggal" value="<?php echo $presensi['tanggal']; ?>" required>

            <label>Mata Kuliah:</label>
            <input type="text" name="matkul" value="<?php echo $presensi['matkul']; ?>" required>

            <button type="submit">Simpan Perubahan</button>
        </form>

        <!-- Formulir untuk menghapus data -->
        <form class="delete-form" action="" method="post">
            <input type="hidden" name="delete_id" value="<?php echo $presensi['id']; ?>">
            <button type="submit" class="delete-button">Hapus Data</button>
        </form>
    </section>
</body>
</html>
