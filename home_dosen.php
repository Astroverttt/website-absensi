<?php
session_start();

// Simulasi data presensi mahasiswa yang berhasil
if (!isset($_SESSION['presensi_mahasiswa'])) {
    $_SESSION['presensi_mahasiswa'] = [
        ['id' => 1, 'nama' => 'Zuda', 'tanggal' => '2023-01-01', 'matkul' => 'Pemrogramman Web'],
        ['id' => 2, 'nama' => 'Feri Irawan Pati', 'tanggal' => '2023-01-02', 'matkul' => 'Pemrogramman Web'],
        // Tambahkan data presensi lainnya sesuai kebutuhan
    ];
}

// Proses penambahan data presensi jika formulir disubmit
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['nama'])) {
    // Ambil data dari formulir (misalnya, $_POST['nama'], $_POST['tanggal'], dll.)
    $nama = $_POST['nama'];
    $tanggal = $_POST['tanggal'];
    $matkul = $_POST['matkul'];

    // Buat data presensi baru
    $id_presensi = count($_SESSION['presensi_mahasiswa']) + 1;
    $presensi_baru = ['id' => $id_presensi, 'nama' => $nama, 'tanggal' => $tanggal, 'matkul' => $matkul];

    // Tambahkan data presensi baru ke dalam sesi
    $_SESSION['presensi_mahasiswa'][] = $presensi_baru;
}

// Proses pembaruan data presensi jika formulir edit disubmit
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['edit_id'])) {
    $edit_id = $_POST['edit_id'];

    // Update data presensi di sesi
    foreach ($_SESSION['presensi_mahasiswa'] as &$p) {
        if ($p['id'] == $edit_id) {
            $p['nama'] = $_POST['nama'];
            $p['tanggal'] = $_POST['tanggal'];
            $p['matkul'] = $_POST['matkul'];
            break;
        }
    }
}

// Proses pembaruan data presensi jika formulir delete disubmit
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['delete_id'])) {
    $delete_id = $_POST['delete_id'];

    // Hapus data presensi dari sesi
    foreach ($_SESSION['presensi_mahasiswa'] as $key => $presensi) {
        if ($presensi['id'] == $delete_id) {
            unset($_SESSION['presensi_mahasiswa'][$key]);
            // Hilangkan elemen yang kosong dari array
            $_SESSION['presensi_mahasiswa'] = array_values($_SESSION['presensi_mahasiswa']);
            // Redirect kembali ke halaman home_dosen.php atau halaman lain
            header("Location: home_dosen.php");
            exit();
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Dosen</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }

        header {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 1em;
        }

        nav {
            background-color: #444;
            color: #fff;
            padding: 1em;
            text-align: center;
        }

        section {
            margin: 1em;
        }

        form {
            max-width: 400px;
            margin: auto;
        }

        label {
            display: block;
            margin-bottom: 8px;
        }

        input {
            width: 100%;
            padding: 8px;
            margin-bottom: 16px;
        }

        button {
            background-color: #333;
            color: #fff;
            padding: 10px 15px;
            border: none;
            cursor: pointer;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 1em;
        }

        table, th, td {
            border: 1px solid #ddd;
        }

        th, td {
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #333;
            color: #fff;
        }

        .delete-form {
            display: inline-block;
        }

        .delete-button {
            background-color: #ff0000;
        }
    </style>
</head>
<body>
    <header>
        <h1>Selamat datang, <?php echo $_SESSION['username']; ?> (Dosen)!</h1>
    </header>
    <nav>
        <a href="#">Presensi Mahasiswa</a> <!-- Tambahkan link ke halaman lain jika diperlukan -->
        <a href="logout.php">Logout</a>
    </nav>
    <section>
        <form action="" method="post">
            <label>Nama Mahasiswa:</label>
            <input type="text" name="nama" required>

            <label>Tanggal Presensi:</label>
            <input type="date" name="tanggal" required>

            <label>Mata Kuliah:</label>
            <input type="text" name="matkul" required>

            <button type="submit">Tambah Presensi</button>
        </form>

        <h2>Data Presensi Mahasiswa</h2>
        <table>
            <thead>
                <tr>
                    <th>Nama Mahasiswa</th>
                    <th>Tanggal Presensi</th>
                    <th>Mata Kuliah</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($_SESSION['presensi_mahasiswa'] as $presensi): ?>
                    <tr>
                        <td><?php echo $presensi['nama']; ?></td>
                        <td><?php echo $presensi['tanggal']; ?></td>
                        <td><?php echo $presensi['matkul']; ?></td>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="edit_id" value="<?php echo $presensi['id']; ?>">
                                <button type="submit">Edit</button>
                            </form>
                            <form class="delete-form" action="" method="post">
                                <input type="hidden" name="delete_id" value="<?php echo $presensi['id']; ?>">
                                <button type="submit" class="delete-button">Delete</button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </section>
</body>
</html>
